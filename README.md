# README #

A simple python3 script that reads a temperature value from a MQTT topic published as JSON string {"temperature":12.34,"humidity":56.78} and shows the temperature as a count of LEDs on a Pimoroni Unicorn phat
