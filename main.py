import time
import unicornhat as unicorn
import json
import paho.mqtt.client as mqtt

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("/demo/climate")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    data = json.loads(msg.payload.decode("utf-8"));
    print(data)
    temp = data['temperature']
    print(temp)
    width,height=unicorn.get_shape()
    print(width,height)
    for y in range(height):
        for x in range(width):
            lampval = y * 8 + x
            brt = int(255 * (temp - lampval) if temp > lampval else 0)
            brt = max(0, min(255, brt))
            unicorn.set_pixel(x,y,brt,brt,brt);
    unicorn.show()

unicorn.set_layout(unicorn.AUTO)
unicorn.rotation(0)
unicorn.brightness(0.5)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("192.168.1.11", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
